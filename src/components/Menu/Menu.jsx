import React from 'react';
import styles from './Menu.scss';



export class Menu extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            series1: [" 1-series "],
            series2: [" 2-series "],
            series3: [" 3-series "],
            series4: [" 4-series "],
            series5: [" 5-series "],
            series6: [" 6-series "],
            series7: [" 7-series "],
            series8: [" 8-series "]
        };
    }






    render() {
        return (
            <nav className={styles.menu}>
                <div>
                    <div className={styles.logo}></div>
                </div>
                <div className={styles.container}>
                    <a href="">{this.state.series1}</a>
                    <a href="">{this.state.series2}</a>
                    <a href="">{this.state.series3}</a>
                    <a href="">{this.state.series4}</a>
                    <a href="">{this.state.series5}</a>
                    <a href="">{this.state.series6}</a>
                    <a href="">{this.state.series7}</a>
                    <a href="">{this.state.series8}</a>
                </div>
            </nav>
        );
    }
}