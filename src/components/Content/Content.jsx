import React from 'react';
import styles from './Content.scss';


export class Content extends React.Component {
    

    render() {
        return (
            <div className={styles.cardContainer}>
                <div className={styles.card}>
                    <div className={styles.card__title}>1-SERIES</div>
                    <div className={styles.ser1}></div>
                </div>
                <div className={styles.card}>
                    <div className={styles.card__title}>2-SERIES</div>
                    <div className={styles.ser2}></div>
                </div>
                <div className={styles.card}>
                    <div className={styles.card__title}>3-SERIES</div>
                    <div className={styles.ser3}></div>
                </div>
                <div className={styles.card}>
                    <div className={styles.card__title}>4-SERIES</div>
                    <div className={styles.ser4}></div>
                </div>
                <div className={styles.card}>
                    <div className={styles.card__title}>5-SERIES</div>
                    <div className={styles.ser5}></div>
                </div>
                <div className={styles.card}>
                    <div className={styles.card__title}>6-SERIES</div>
                    <div className={styles.ser6}></div>
                </div>
                <div className={styles.card}>
                    <div className={styles.card__title}>7-SERIES</div>
                    <div className={styles.ser7}></div>
                </div>
                <div className={styles.card}>
                    <div className={styles.card__title}>8-SERIES</div>
                    <div className={styles.ser8}></div>
                </div>
            </div>
        );
    }


}