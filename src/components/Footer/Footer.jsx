import React from 'react';
import styles from './Footer.scss';

export class Footer extends React.Component  {
    render() {
        return (
            <div className={styles.footer}>
                <div className={styles.text}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Non, odio.</div>
            </div>
        );
    }
}
