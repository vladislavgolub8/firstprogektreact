import ReactDOM from 'react-dom';
import React from 'react';
import { Menu } from './components/Menu/Menu';
import { Content } from './components/Content/Content';
import { Footer } from './components/Footer/Footer';


const propsValues = {
  title: 'List',
  items: ['1', 2]
};

ReactDOM.render(
  <div>
    <Menu />
    <Content/>
    <Footer/>
  </div>,
  document.getElementById('app')
);
